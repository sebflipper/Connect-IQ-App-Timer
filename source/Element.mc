class Element {
	var id;
	var position;
	var name;
	var time;
	var autoRestart;
	var beep;
	var beep5s;
	var vibrate;
	
	function initialize (position,json) {
		self.id = extractValue("id", json);
		self.position=position;	
		self.name = extractValue("name",json);
		var timeString = extractValue("time",json);
		if(timeString.equals("")){
			time=60;
		}else{
			time=timeString.toNumber();
		}
		self.beep = !extractValue("beep",json).equals("false")?true:false;
		self.beep5s = !extractValue("beep5s",json).equals("false")?true:false;
		self.vibrate = !extractValue("vibrate",json).equals("false")?true:false;
		self.autoRestart = extractValue("autoRestart",json).equals("true")?true:false;
	}
	
	function setPosition(position){
		self.position=position;
	}
	
	function extractValue(key,json){
		var index =json.find("\""+key+"\":");
		if(index != null){
			var value = json.substring(index ,json.length());
			var indexEnd = value.find("\",");
			if(indexEnd == null){
				indexEnd =value.length()-2;
			}
			value = value.substring(value.find(":\"")+2,indexEnd);
			return value;
		}
		return "";
	}
	
	function getProperty(property){
		if(property.equals("id")){
		 return id;
		}else if(property.equals("name")){
		 return name;
		}else if(property.equals("time")){
			return time;
		}else if(property.equals("beep")){
			return beep;
		}else if(property.equals("beep5s")){
			return beep5s;
		}else if(property.equals("vibrate")){
			return vibrate;
		}else if(property.equals("autoRestart")){
			return autoRestart;
		}else{
			return null;
		}
	}
	
	function toJson(){
		return "{\"id\":\""+id+"\",\"name\":\""+name+"\",\"time\":\""+time+"\",\"beep\":\""+beep+"\",\"beep5s\":\""+beep5s+"\",\"vibrate\":\""+vibrate+"\",\"autoRestart\":\""+autoRestart+"\"}";
	}
}