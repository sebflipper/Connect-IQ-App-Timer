using Toybox.WatchUi as Ui;

class TouchTimePickerDelegate extends Ui.BehaviorDelegate {

	hidden var data;

	function initialize(data) {
		BehaviorDelegate.initialize();
		data.time="";
		self.data=data;
	}

	function onBack() {
		setTimer();
		Ui.popView(Ui.SLIDE_RIGHT);
		return true;
	}

	function onKey(keyEvent) {
		if (keyEvent.getKey() == 4 && self.data.time.toString().length() != 0) {
			startTimer();
		}
		return true;
	}

	function addNumber(no) {
		if (data.time.length() < 6) {
			data.time = data.time + no;
			Ui.requestUpdate();
		}
	}

	function setTimer() {
		if (data.time.length() == 0) {
			data.time = "1";
		}
		var length = data.time.length();

		if (length <= 2) {
			data.time = data.time.toNumber() * 60;
			return;
		}

		if (length >= 5) {
			var zerosToAdd = 6 - length;
			for (var idx = 0; idx < zerosToAdd; idx++) {
				data.time = data.time + "0";
			}

			var numbers = data.time.toCharArray();
			var hours = (numbers[0] + numbers[1]).toNumber();
			var mins = (numbers[2] + numbers[3]).toNumber();
			var secs = (numbers[4] + numbers[5]).toNumber();

			data.time = (hours * 60 * 60) + (mins * 60) + secs;
			return;
		}

		var zerosToAdd = 4 - length;
		for (var idx = 0; idx < zerosToAdd; idx++) {
			data.time = data.time + "0";
		}

		var numbers = data.time.toCharArray();
		var mins = (numbers[0] + numbers[1]).toNumber();
		var secs = (numbers[2] + numbers[3]).toNumber();

		data.time = (mins * 60) + secs;
	}

	function startTimer() {
		setTimer();

		Ui.popView(Ui.SLIDE_IMMEDIATE);

		var model = new TimerModel(data);
		models.put(data.id, model);
		model.startTimer();
		Ui.pushView(new TimerView(data), new TimerDelegate(data), Ui.SLIDE_IMMEDIATE);
	}

	function onTap(clickEvent) {
		var cords = clickEvent.getCoordinates();
		var x = cords[0];
		var y = cords[1];

		if (
				x >= 103 && x <= 155 && 
				y >= 4 && y <= 53
		) {
			addNumber("0");
		} else if (
				x >= 168 && x <= 207 && 
				y >= 18 && y <= 71
		) {
			addNumber("1");
		} else if (
				x >= 208 && x <= 248 && 
				y >= 66 && y <= 120
		) {
			addNumber("2");
		} else if (
				x >= 199 && x <= 240 && 
				y >= 131 && y <= 194
		) {
			addNumber("3");
		} else if (
				x >= 163 && x <= 215 && 
				y >= 182 && y <= 223
		) {
			addNumber("4");
		} else if (
				x >= 104 && x <= 160 && 
				y >= 201 && y <= 255
		) {
			addNumber("5");
		} else if (
				x >= 55 && x <= 94 && 
				y >= 175 && y <= 250
		) {
			addNumber("6");
		} else if (
				x >= 1 && x <= 50 && 
				y >= 130 && y <= 185
		) {
			addNumber("7");
		} else if (
				x >= 27 && x <= 56 && 
				y >= 51 && y <= 120
		) {
			addNumber("8");
		} else if (
				x >= 43 && x <= 92 && 
				y >= 35 && y <= 72
		) {
			addNumber("9");
		} else if (
				x >= 103 && x <= 154 && 
				y >= 64 && y <= 102
		) {
			data.time="";
			Ui.requestUpdate();
		} else if (
				x >= 97 && x <= 166 && 
				y >= 158 && y <= 192
		) {
			startTimer();
		}

		return true;
	}
}