using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class AlarmMenu extends Menu {

	hidden var alarms;
	hidden var bip;
	hidden var bip5S;
	hidden var vibrate;
	
	function initialize (data) {
        alarms = Ui.loadResource(Rez.Strings.Alarms);
        bip = Ui.loadResource(Rez.Strings.Bip);
        bip5S = Ui.loadResource(Rez.Strings.Bip5s);
        vibrate = Ui.loadResource(Rez.Strings.Vibrate);
		
		var menuItems = new [0];
    	if(Attention has :playTone){
			menuItems.add(new StateMenuItem (:Beep,bip,data,"beep"));
			menuItems.add(new StateMenuItem (:Beep5s,bip5S,data,"beep5s"));
		}
		if(Attention has :vibrate){
			menuItems.add(new StateMenuItem (:Vibrate,vibrate,data,"vibrate"));
		}
		    
		Menu.initialize (menuItems, alarms);
	}
	
}