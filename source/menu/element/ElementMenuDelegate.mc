using Toybox.WatchUi as Ui;

class ElementMenuDelegate extends MenuDelegate {

	hidden var timer;

    function initialize(menu) {
        MenuDelegate.initialize(menu);
		timer = Ui.loadResource(Rez.Strings.Timer);
    }

    function onMenuItem(item) {
    	if(item.id==:Continu){
    		Ui.popView (Ui.SLIDE_LEFT);
    	}else if(item.id==:Reset){
    		 models.get(item.data.id).resetTimer();
    	}else if(item.id==:Start){
    		var model;
    		if(models.hasKey(item.data.id)){
    			model = models.get(item.data.id);
    		}else{
    			model = new TimerModel(item.data);
    			models.put(item.data.id,model);
    		}
    		model.startTimer();
				Ui.pushView(new TimerView(item.data), new TimerDelegate(item.data), Ui.SLIDE_IMMEDIATE);
    	}else if(item.id ==:Timer){
    		Ui.pushView(new TimePicker(timer,item.data), new TimePickerDelegate(item.data), Ui.SLIDE_IMMEDIATE);
    	}else if(item.id ==:Remove){
    		if (models.hasKey(item.data.id)) {
    			var timer = models.get(item.data.id);
					timer.resetTimer();
    		}

    		elements.remove(item.data);
    		Ui.popView (Ui.SLIDE_LEFT);
    	}else if(item.id ==:Rename){
				if (Ui has :TextPicker) {
					Ui.pushView(new Ui.TextPicker(item.data.name), new EditElementPicker(item.data), Ui.SLIDE_LEFT);
				}
    	}else if ( item.id == :Alarms){
		    var menu = new AlarmMenu (item.data);
				Ui.pushView(menu,new AlarmMenuDelegate(menu) ,  Ui.SLIDE_LEFT );
    	}else if ( item.id == :Up){
		    var position=item.data.position;
		   	if(position>0){
		   		var temps= new [0];
		    	for(var i=0;i<elements.size();i++){
			    	if(i==position-1){
			    		item.data.setPosition(temps.size());
			    		temps.add(item.data);
		    		}
		    		if(i!=position){
		    			elements[i].setPosition(temps.size());
		    			temps.add(elements[i]);
		   			}
		   		}
		   		elements= new [0];
		    	elements.addAll(temps);
		   	}		   
		    Ui.popView (Ui.SLIDE_LEFT);
    	}else if ( item.id == :Down){
		    var position=item.data.position;
		    if(position<elements.size()-1){
		    	var temps= new [0];
		    	for(var i=0;i<elements.size();i++){
		    		if(i!=position){
		    			elements[i].setPosition(temps.size());
		    			temps.add(elements[i]);
		    		}
		    		if(i==position+1){
		    			item.data.setPosition(temps.size());
		    			temps.add(item.data);
		    		}		    	
		    	}
		    	elements= new [0];
		    	elements.addAll(temps);
			}
		    Ui.popView (Ui.SLIDE_LEFT);
    	}else if(item.id ==:AutoRestart){
    		item.data.autoRestart=!item.data.autoRestart;
    	}
    }
}