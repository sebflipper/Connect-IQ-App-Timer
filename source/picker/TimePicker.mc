using Toybox.Application as App;
using Toybox.Graphics as Gfx;
using Toybox.WatchUi as Ui;

class TimePicker extends Ui.Picker {

	hidden var data;
    function initialize(titleS,data) {
		self.data=data;
        var title = new Ui.Text({:text=>titleS, :locX=>Ui.LAYOUT_HALIGN_CENTER, :locY=>Ui.LAYOUT_VALIGN_BOTTOM, :color=>Gfx.COLOR_WHITE});
 
        var factories = new [5];
        factories[0] = new NumberFactory(0, 99, 1, {});
        factories[1] = new Ui.Text({:text=>":", :font=>Gfx.FONT_MEDIUM, :locX =>Ui.LAYOUT_HALIGN_CENTER, :locY=>Ui.LAYOUT_VALIGN_CENTER, :color=>Gfx.COLOR_WHITE});
        factories[2] = new NumberFactory(0, 59, 1, {});
        factories[3] = new Ui.Text({:text=>":", :font=>Gfx.FONT_MEDIUM, :locX =>Ui.LAYOUT_HALIGN_CENTER, :locY=>Ui.LAYOUT_VALIGN_CENTER, :color=>Gfx.COLOR_WHITE});
        factories[4] = new NumberFactory(0, 59, 1, {});

        var defaults = splitStoredTime(factories.size());
        defaults[0] = factories[0].getIndex(defaults[0].toNumber());
        defaults[2] = factories[2].getIndex(defaults[2].toNumber());
        defaults[4] = factories[4].getIndex(defaults[4].toNumber());
        
        Picker.initialize({:title=>title, :pattern=>factories, :defaults=>defaults});
    }

    function onUpdate(dc) {
        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_BLACK);
        dc.clear();
        Picker.onUpdate(dc);
    }

    function splitStoredTime(arraySize) {
        var storedValue = data.time;
        var defaults = new [arraySize];
        var seconds = storedValue % 60;
		var minutes = (storedValue / 60) % 60;
		var hours = storedValue / 60 /60;
		defaults[0] = hours;
		defaults[2] = minutes;
		defaults[4]= seconds;
        return defaults;
    }
}

class TimePickerDelegate extends Ui.PickerDelegate {

	hidden var data;
    function initialize(data) {
    	self.data=data;
        PickerDelegate.initialize();
    }

    function onCancel() {
        Ui.popView(Ui.SLIDE_RIGHT);
    }

    function onAccept(values) {
        var time = values[0] *60*60 + values[2]*60 + values[4];
        data.time=time;

        Ui.popView(Ui.SLIDE_IMMEDIATE);

        var model = new TimerModel(data);
        models.put(data.id, model);
        model.startTimer();
        Ui.pushView(new TimerView(data), new TimerDelegate(data), Ui.SLIDE_IMMEDIATE);
    }
}
